#!/u:sr/bin/env python3
import connexion
import datetime
import logging
import json

from connexion import NoContent
policy_type={}
policy_type2value={}

def printPolicy():
	print(json.dumps(policy_type, sort_keys=True, indent=4))
	print(json.dumps(policy_type2value, sort_keys=True, indent=4))

def console():
	return ([policy_type,policy_type2value], 200)

def reset():
	policy_type.clear()
	policy_type2value.clear()
	return ('ok',200)

def get_healthcheck():
	return ('OK!', 200)

def get_all_policy_types():
	printPolicy()
	return (list(policy_type2value.keys()), 200)

def get_policy_type(policy_type_id):
	if policy_type_id in policy_type:
		return (policy_type2value[policy_type_id], 200)
	else:
		return ('policy type not found. {}'.format(policy_type_id), 404)

def delete_policy_type(policy_type_id):
	if policy_type_id in policy_type:
		print(len(policy_type[policy_type_id]))
		if len(policy_type[policy_type_id])>0:
			return ('Policy type cannot be deleted because there are instances All instances must be removed before a policy type can be deleted',400)
		else:
			policy_type.pop(policy_type_id)
			policy_type2value.pop(policy_type_id)
			return ('policy type successfully deleted. {} deleted'.format(policy_type_id), 204)
	else:
		return ('policy type not found:{}'.format(policy_type_id),404)

def create_policy_type(policy_type_id, body):
	if policy_type_id in policy_type:
		return ('illegal ID, or object already existed', 400)
	else:
		policy_type[policy_type_id]={}
		policy_type2value[policy_type_id]=(body)
		return ('{} created'.format(body), 201)

def get_policy_instance_status(policy_type_id,policy_instance_id):
	if policy_type_id in policy_type and policy_instance_id in policy_type[policy_type_id]:
		return (policy_type[policy_type_id][policy_instance_id],200)
	else:
		return ('there is no policy instance with this policy_instance_id or there is no policy type with this policy_type_id', 404)

def delete_policy_instance(policy_type_id,policy_instance_id):
	if policy_type_id in policy_type and policy_instance_id in policy_type[policy_type_id]:
		policy_type[policy_type_id].pop(policy_instance_id)
		return ('policy instance successfully deleted. {}, {} deleted'.format(policy_type_id,policy_instance_id), 204)
	else:
		return ('there is no policy instance with this policy_instance_id or there is no policy type with this policy_type_id', 404)

def create_or_replace_policy_instance(policy_type_id,policy_instance_id,body):
	if policy_type_id in policy_type:
		policy_type[policy_type_id][policy_instance_id]=(body)
		return (policy_type[policy_type_id][policy_instance_id],201)
	else:
		return ('There is no policy type with this policy_type_id', 404)

def get_policy_instance(policy_type_id,policy_instance_id):
	if policy_type_id in policy_type and policy_instance_id in policy_type[policy_type_id]:
		return (policy_type[policy_type_id][policy_instance_id],200)
	else:
		return ('there is no policy instance with this policy_instance_id or there is no policy type with this policy_type_id', 404)

def get_all_instances_for_type(policy_type_id):
	return (list(policy_type[policy_type_id].keys()),200)