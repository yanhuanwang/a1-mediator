Build/run docker

docker build -t connapp:latest .

docker run -p8080:8080 connapp


Open in browser
http://localhost:8080/a1-p/healthcheck

Swagger UI console:
http://localhost:8080/ui
