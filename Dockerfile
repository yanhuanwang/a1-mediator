
FROM python:3

WORKDIR /usr/src/app

RUN pip install connexion[swagger-ui]

COPY a1.py a1.py
COPY tst.py tst.py
COPY a1_mediator_0.11.0.yaml a1_mediator_0.11.0.yaml

CMD [ "python", "./tst.py" ]