import connexion

app = connexion.App(__name__, specification_dir='.')
app.add_api('a1_mediator_0.11.0.yaml')
app.run(port=8080)
